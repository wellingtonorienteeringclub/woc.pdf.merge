﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace WOC.PDF.Merge {
  class Program {
    static void Main(string[] args) {
      var groups = JsonConvert.DeserializeObject<List<Group>>(File.ReadAllText("settings.json"));

      foreach (var group in groups) {
        Merge(group);
      }

      Console.WriteLine("Done!");
      Console.ReadLine();
    }

    private static void Merge(Group group) {
      using (MemoryStream stream = new MemoryStream()) {
        Document doc = new Document();
        PdfCopy pdf = new PdfCopy(doc, stream) {
          CloseStream = false
        };
        doc.Open();

        PdfReader reader = null;
        PdfImportedPage page = null;

        foreach (var file in group.Items) {
          Console.WriteLine($"Adding {file.Path}.");

          reader = new PdfReader(file.Path);
          for (int i = 0; i < reader.NumberOfPages; i++) {
            page = pdf.GetImportedPage(reader, i + 1);

            for (var j = 0; j < file.Copies; j++) {
              pdf.AddPage(page);
            }
          }

          pdf.FreeReader(reader);
          reader.Close();
        }

        doc.Close();

        using (FileStream streamX = new FileStream(group.Out, FileMode.Create)) {
          stream.WriteTo(streamX);
        }
      }
    }

    public class Group {
      public string Out { get; set; }
      public List<Item> Items { get; set; }
    }

    public class Item {
      public string Path { get; set; }
      public int Copies { get; set; }
    }
  }
}